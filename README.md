# DecoratorPattern

The Decorator Pattern comes under the Structural pattern. By using the decorator pattern we can modify the functionality of an object dynamically. The functionality of an object is extended by adding additional coverage around an object. If a number of objects are present for a single class, we can modify the behavior of a single object without affecting the other instances of the same class.

## Real World Decorators: Java I/O

The large number of classes in the java.io package is... overwhelming. Don’t feel alone
if you said “whoa” the fi rst (and second and third) time you looked at this API. But
now that you know the Decorator Pattern, the I/O classes should make more sense
since the java.io package is largely based on Decorator. 

>Example of JAVA I/O
---
![Decorator Pattern in I/O Class!](/images/io.png "I/O class example of Decorator Pattern")
public class StarbuzzCoffee {

    public static void main(String[] args) {
        Beverage beverage = new Espresso();
        System.out.println("Simple Respresso Coffee!");
        System.out.println("Description: " + beverage.getDescription() + " || Cost: " + beverage.cost());

        System.out.println("2nd Bevarage(Coffee) with add-ons!");
        Beverage beverage2 = new DarkRoast();
        System.out.println("#1 Add-on: " + beverage2.getDescription() + " || Cost: " + beverage2.cost());

        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println("#2 Add-on: " + beverage2.getDescription() + " || Cost: " + beverage2.cost());

        beverage2 = new Soy(beverage2);
        System.out.println("#3 Add-on: " + beverage2.getDescription() + " || Cost: " + beverage2.cost());

    }

}
